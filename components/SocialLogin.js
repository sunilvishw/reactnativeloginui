import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function SocialLogin() {
  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>Sign up with</Text>
      <View style={styles.inner_container}>
        <View style={styles.buttonContainer}>
          <Icon.Button name="google" backgroundColor="#fafefe" color="#000">
            <Text style={{ fontFamily:'Montserrat_400Regular', textTransform:'uppercase',fontWeight: 'bold' }}>Google</Text>
          </Icon.Button>
        </View>
        <View style={styles.buttonContainer}>
          <Icon.Button name="facebook" backgroundColor="#fafefe" color="#000">
            <Text style={{  fontFamily:'Montserrat_400Regular', textTransform:'uppercase',fontWeight: 'bold'}}>Facebook</Text>
          </Icon.Button>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding : 30,
    backgroundColor: '#fff',
   
  },
  inner_container: {
    flexDirection: 'row',
    marginTop: 10,
  },
  buttonContainer: {
    margin: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  paragraph:{
    color:'#8693a9',
    fontFamily: 'Montserrat_400Regular',
  }
});
