import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function LoginForm() {
  return (
    <View style={styles.container}>
      <View style={ styles.centerView}>
      <Text style={styles.paragraph}>
        Or sign up with classic way
        </Text>
    </View>
    <View style={styles.inputContainer}>
      <Icon name="person" size={20} color="#000" style={styles.iconStyle} />
      <TextInput
        style={styles.inputStyle}
        autoCorrect={false}
        placeholder="Name"
      />
    </View>
    <View style={styles.inputContainer}>
      <Icon name="email" size={20} color="#000" style={styles.iconStyle} />
      <TextInput
        style={styles.inputStyle}
        autoCorrect={false}

        placeholder="Email"
      />
    </View>

    <View style={styles.inputContainer}>
      <Icon name="lock" size={20} color="#000" style={styles.iconStyle} />
      <TextInput
        style={styles.inputStyle}
        autoCorrect={false}
        secureTextEntry
        placeholder="Password"
      />
    </View>

    <Text style={styles.message}>
      <Text style={styles.paragraph}>password strength :</Text>
      <Text style={{ fontWeight: 'bold', color: 'green' }}> strong</Text>
    </Text>

    <View style={styles.buttonContainer, styles.centerView}>
      <TouchableOpacity style={styles.submit}>
        <Text style={styles.text}>Create Account</Text>
      </TouchableOpacity>
    </View>
      
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 24,
    backgroundColor: 'rgb(227, 228, 230)',

  },

  inputContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginTop: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    fontFamily: 'Montserrat_400Regular',
    elevation: 1,
  },
  inputStyle: {
    flex: 1,
    fontFamily: 'Montserrat_400Regular',
  },
  submit: {
    backgroundColor: '#6071e3',
    padding: 10,
    margin: 15,
    fontFamily: 'Montserrat_400Regular',
    borderRadius: 4,
  },
  text: {
    color: '#fff',
    fontFamily: 'Montserrat_400Regular',
  },

  iconStyle: {
    padding: 10,
  },
  message: {
    marginTop: 30,
    justifyContent: 'flex-start',
    fontFamily: 'Montserrat_400Regular',
  },
  centerView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  paragraph: {
    color: '#8693a9',
    fontFamily: 'Montserrat_400Regular',

  }
});
