import * as React from 'react';
import { View, StyleSheet} from 'react-native';
import {AppLoading} from 'expo';
import LoginForm from './components/LoginForm';
import SocialLogin from './components/SocialLogin';
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts, Montserrat_400Regular } from '@expo-google-fonts/montserrat';

export default function App() {

  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }




    return (


     
      <View style={styles.container}>
        <LinearGradient
          colors={['#2193b0', '#5dd5ed']}
          style={{flex:1}}
          >
          <View style={styles.container_inner_view_spacing}>
            <SocialLogin />
            <LoginForm />
          </View>
        </LinearGradient>
      </View>



    );
  
}

const styles = StyleSheet.create({
  container: {
    fontFamily: 'Montserrat_400Regular',
flex:1
  }
  ,
  container_inner_view_spacing: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 40,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 40,
  }
});